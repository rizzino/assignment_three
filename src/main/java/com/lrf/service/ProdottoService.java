package com.lrf.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.lrf.domain.CantinaProduttrice;
import com.lrf.domain.Grappa;
import com.lrf.domain.Regione;
import com.lrf.domain.Utente;
import com.lrf.domain.UtenteGrappa;
import com.lrf.domain.UtenteVino;
import com.lrf.domain.Vino;
import com.lrf.repository.CantinaProduttriceRepository;
import com.lrf.repository.GrappaRepository;
import com.lrf.repository.RegioneRepository;
import com.lrf.repository.UtenteGrappaRepository;
import com.lrf.repository.UtenteRepository;
import com.lrf.repository.UtenteVinoRepository;
import com.lrf.repository.VinoRepository;

@Service
public class ProdottoService {
	
	@Autowired
	UtenteRepository utentiDB;
	
	@Autowired
	VinoRepository viniDB;
	
	@Autowired
	GrappaRepository grappeDB;
	
	@Autowired
	RegioneRepository regioniDB;
	
	@Autowired
	CantinaProduttriceRepository cantineProdDB;
	
	@Autowired
	UtenteVinoRepository utentiViniDB;
	
	@Autowired
	UtenteGrappaRepository utentiGrappeDB;
	
	/**
	 * Metodo per l'aggiunta di un vino
	 * @param model Modello UI
	 * @param nome Nome
	 * @param denominazione Denominazione
	 * @param nazionalita Nazionalità
	 * @param regione Regione del vino
	 * @param cantinaProduttrice Cantina produttrice
	 * @param tipologia Tipologia
	 * @param select Valore della select all'invio della form
	 */
	@Transactional
	public void addVino(Model model, String nome, String denominazione,
			String nazionalita, String regione, String cantinaProduttrice,
			String tipologia, String select) {
		
		CantinaProduttrice cantp = cantineProdDB.findByNome(cantinaProduttrice);
		Vino vino = null;
		if(cantp!=null) {
			vino = viniDB.findByNomeAndIdCantina(nome, cantp);
			if(vino != null) {
				setError(model,"Vino già esistente.");
				return;
			}
		}else {
			cantp = new CantinaProduttrice(cantinaProduttrice);
			cantineProdDB.saveAndFlush(cantp);
		}			
				
		if(select.equalsIgnoreCase("it")) {
			Regione reg = regioniDB.findByNome(regione);
			if(reg == null) {
				setError(model, "Regione non trovata.");
				return;
			}
			vino = new Vino(nome, denominazione, "Italiana", reg, cantp, tipologia);
		}
		if(select.equalsIgnoreCase("st")) {
			Regione reg = null;
			vino = new Vino(nome, denominazione, nazionalita, reg, cantp, tipologia);
		}
		
		viniDB.saveAndFlush(vino);
    	model.addAttribute("response","Vino aggiunto correttamente.");	
	}
	
	
	/**
	 * Metodo per l'update di un vino
	 * @param model
	 * @param nuovoNome Nuovo nome del vino
	 * @param denominazione Denominazione
	 * @param editNazionalita Nazionalità
	 * @param editRegione Regione
	 * @param cantinaProduttrice Cantina produttrice
	 * @param tipologia Tipologia
	 * @param oldNome Nome del vino eventualmente modificato
	 * @param select Valore della select all'invio della form
	 */
	@Transactional
	public void updateVino(Model model, String nuovoNome, String denominazione, 
			String editNazionalita, String editRegione, String cantinaProduttrice,
			String tipologia, String oldNome, String oldCantina, String select) {
		
		CantinaProduttrice cantinaProd = cantineProdDB.findByNome(oldCantina);
		if(cantinaProd==null) {
			setError(model, "Cantina non trovata.");
			return;
		}
		
		Vino vino = viniDB.findByNomeAndIdCantina(oldNome, cantinaProd);	
		if(vino == null) {
			setError(model, "Vino non trovato.");
			return;
		}
		else {
			if(!(oldNome.equals(nuovoNome) && oldCantina.equals(cantinaProduttrice))) {
				CantinaProduttrice nuovaCantinaProduttrice = cantineProdDB.findByNome(cantinaProduttrice);
				if(nuovaCantinaProduttrice==null) {
					CantinaProduttrice newCantina = new CantinaProduttrice(cantinaProduttrice);
					cantineProdDB.saveAndFlush(newCantina);
					vino.setIdCantina(newCantina);
				}
				else {
					for (int i = 0; i < nuovaCantinaProduttrice.getVini().size(); i++) {
						if(nuovaCantinaProduttrice.getVini().get(i).getNome().equalsIgnoreCase(nuovoNome)) {
							setError(model, "Vino già presente in questa cantina.");
							return;
						}
					}
					
					vino.setIdCantina(nuovaCantinaProduttrice);
	
				}
			}
		}
		
		if(!vino.getDenominazione().equalsIgnoreCase(denominazione)) {
			vino.setDenominazione(denominazione);
		}
		
		if(select.equalsIgnoreCase("it")) {
			Regione regione = regioniDB.findByNome(editRegione);
			if(regione == null) {
				setError(model, "Regione non trovata.");
				return;
			}
			else {
				vino.setNazionalita("Italiana");
				vino.setIdRegione(regione);
			}
		}
		else {
			if(select.equalsIgnoreCase("st")) {
				vino.setNazionalita(editNazionalita);
				vino.setIdRegione(null);
			}
		}
		
		boolean flag = false;
		if(tipologia.equalsIgnoreCase("r"))	flag = vino.setTipologiaVino("ROSSO");
		if(tipologia.equalsIgnoreCase("b"))	flag = vino.setTipologiaVino("BIANCO");
		if(!flag) {
			setError(model, "Errore di tipologia.");
			return;
		}
		
		if(!nuovoNome.equals(oldNome)) {
			vino.setNome(nuovoNome);
		}
		
		vino.setDenominazione(denominazione);
		viniDB.saveAndFlush(vino);
		
	}
	
	/**
	 * Metodo per la delete di un vino
	 * @param id ID del vino da eliminare
	 * @param model
	 */
	@Transactional
	public void deleteVino(Long id, Model model) {
		
		Vino vino = viniDB.findById(id).get();
		
		if(vino==null) {
			setError(model, "Vino non trovato.");
			return;
		}else {
			List<UtenteVino> list = utentiViniDB.findByVino(vino);
			for(UtenteVino relazione: list) {
				utentiViniDB.delete(relazione);
			}
			utentiViniDB.flush();
			viniDB.deleteById(id);
			viniDB.flush();
		
		}
	}
	
	/**
	 * Metodo per l'aggiunta del vino alla cantina di un utente
	 * @param model
	 * @param utente Utente che sta aggiungendo vino alla sua cantina
	 * @param nomevino Nome del vino
	 * @param anno Anno del vino
	 * @param nbottiglie Numero di bottiglie di vino aggiunte
	 */
	@Transactional
	public void addVinoToCantina(Model model, Utente utente, String nomevino, String anno, String nbottiglie) {
		
		Vino vino = viniDB.findByNome(nomevino);
		if(vino==null){
			setError(model, "Vino non corretto.");
			return;
		}else{
			UtenteVino cantina = utentiViniDB.findByUtenteAndAnnoAndVino(utente, Integer.parseInt(anno), vino);
			if(cantina == null) {
				UtenteVino tmp = new UtenteVino(utente, vino, Integer.parseInt(anno), Integer.parseInt(nbottiglie));
				utentiViniDB.saveAndFlush(tmp);
			}else{
				cantina.setnBottiglie(cantina.getnBottiglie()+Integer.parseInt(nbottiglie));
				model.addAttribute("response", "success");
			}			
		}
		
	}

	/**
	 * Metodo per l'aggiunta di una grappa
	 * @param model Modello UI
	 * @param nome Nome
	 * @param denominazione Denominazione
	 * @param nazionalita Nazionalità
	 * @param regione Regione della grappa
	 * @param cantinaProduttrice Cantina produttrice
	 * @param tipologia Tipologia
	 * @param select Valore della select all'invio della form
	 */
	@Transactional
	public void addGrappa(Model model, String nome, String denominazione,
			String nazionalita, String regione, String cantinaProduttrice,
			String tipologia, String select) {
		
		CantinaProduttrice cantp = cantineProdDB.findByNome(cantinaProduttrice);
		Grappa grappa = null;
		
		if (cantp != null) {
			grappa = grappeDB.findByNomeAndIdCantina(nome, cantp);
			if(grappa != null) {
				setError(model,"Grappa già esistente.");
				return;
			}
		}else {
			cantp = new CantinaProduttrice(cantinaProduttrice);
			cantineProdDB.saveAndFlush(cantp);
		}
		
		if(select.equalsIgnoreCase("it")) {
			Regione reg = regioniDB.findByNome(regione);
			if(reg == null) {
				setError(model, "Regione non trovata.");
				return;
			}
			grappa = new Grappa(nome, denominazione, "Italiana", reg, cantp, tipologia);
		}
		if(select.equalsIgnoreCase("st")) {
			Regione reg = null;
			grappa = new Grappa(nome, denominazione, nazionalita, reg, cantp, tipologia);
		}
		
		grappeDB.saveAndFlush(grappa);
		
		model.addAttribute("response", "Grappa aggiunta correttamente.");
	}
	
	
	/**
	 * Metodo per l'update di una grappa
	 * @param model
	 * @param nuovoNome Nuovo nome della grappa
	 * @param denominazione Denominazione
	 * @param editNazionalita Nazionalità
	 * @param editRegione Regione
	 * @param cantinaProduttrice Cantina produttrice
	 * @param tipologia Tipologia
	 * @param oldNome Nome del vino eventualmente modificato
	 * @param select Valore della select all'invio della form
	 */
	@Transactional
	public void updateGrappa(Model model, String nuovoNome, String denominazione, 
			String editNazionalita, String editRegione, String cantinaProduttrice,
			String tipologia, String oldNome, String oldCantina, String select) {
		
		CantinaProduttrice cantinaProd = cantineProdDB.findByNome(oldCantina);
		if(cantinaProd==null) {
			setError(model, "Cantina non trovata.");
			return;
		}
		
		Grappa grappa = grappeDB.findByNomeAndIdCantina(oldNome, cantinaProd);	
		if(grappa == null) {
			setError(model, "Grappa non trovata.");
			return;
		}
		else {
			if(!(oldNome.equals(nuovoNome) && oldCantina.equals(cantinaProduttrice))){
				CantinaProduttrice nuovaCantinaProduttrice = cantineProdDB.findByNome(cantinaProduttrice);
				if(nuovaCantinaProduttrice==null) {
					CantinaProduttrice newCantina = new CantinaProduttrice(cantinaProduttrice);
					cantineProdDB.saveAndFlush(newCantina);
					grappa.setIdCantina(newCantina);
				}
				else {
					for (int i = 0; i < nuovaCantinaProduttrice.getGrappa().size(); i++) {
						if(nuovaCantinaProduttrice.getGrappa().get(i).getNome().equalsIgnoreCase(nuovoNome)) {
							setError(model, "Grappa già presente in questa cantina.");
							return;
						}
					}
					
					grappa.setIdCantina(nuovaCantinaProduttrice);
	
				}
			}
		}
		
		if(!grappa.getDenominazione().equalsIgnoreCase(denominazione)) {
			grappa.setDenominazione(denominazione);
		}
		
		if(select.equalsIgnoreCase("it")) {
			Regione regione = regioniDB.findByNome(editRegione);
			if(regione == null) {
				setError(model, "Regione non trovata.");
				return;
			}
			else {
				grappa.setNazionalita("Italiana");
				grappa.setIdRegione(regione);
			}
		}
		else {
			if(select.equalsIgnoreCase("st")) {
				grappa.setNazionalita(editNazionalita);
				grappa.setIdRegione(null);
			}
		}
		
		boolean flag = false;
		if(tipologia.equalsIgnoreCase("ba"))	flag = grappa.setTipologiaGrappa("BARRICATA");
		if(tipologia.equalsIgnoreCase("bi"))	flag = grappa.setTipologiaGrappa("BIANCA");
		if(!flag) {
			setError(model, "Errore di tipologia.");
			return;
		}
		
		if(!nuovoNome.equals(oldNome)) {
			grappa.setNome(nuovoNome);
		}
		
		grappa.setDenominazione(denominazione);
		grappeDB.saveAndFlush(grappa);
		
	}
		
	
	/**
	 * Metodo per la delete di una grappa
	 * @param id ID della grappa da eliminare
	 * @param model
	 */
	@Transactional
	public void deleteGrappa(Long id, Model model) {
		
		Grappa grappa = grappeDB.findById(id).get();
		
		if(grappa == null) {
			setError(model, "Grappa non trovata.");
			return;
		}else {
			List<UtenteGrappa> list = utentiGrappeDB.findByGrappa(grappa);
			for(UtenteGrappa relazione: list) {
				utentiGrappeDB.delete(relazione);
			}
			utentiGrappeDB.flush();
			grappeDB.deleteById(id);
			grappeDB.flush();
			
		}
		
	}
	
	public final void setError(Model model, String message) {
		model.addAttribute("error", true);
		model.addAttribute("message", message);
	}
	
}
