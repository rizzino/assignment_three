package com.lrf.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lrf.request.WrapperProdottoUtente;
import com.lrf.service.UtenteService;



@Controller
@RequestMapping("/utente")
public class UtenteController {
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
    private UtenteService utenteservice;
	
	
	
	@GetMapping("")
	public String listAll(Model model, @CookieValue(value="mail", required=false) String mail,
			 @CookieValue(value="Role", required=false) String role) {
		
		utenteservice.findAll(model, mail, role);
		return "allUtenti";
	}
	
	@PostMapping("/add")
	public void addUtente(@RequestParam("nome") String nome,
			@RequestParam("mail") String mail,
			@RequestParam("cognome") String cognome, Model model) {
		
		utenteservice.addUtente(model, nome, cognome, mail);
	}
	
	
	@PostMapping("/update")
	public String updateUtente(@RequestParam("id") String id, @RequestParam("name") String nome,
			@RequestParam("lastname") String cognome,
			@RequestParam("mail") String nuovaMail,
			@CookieValue(value="Role") String role,
			@CookieValue(value="Mail") String mail,Model model) {
		
		utenteservice.updateUtente(model, Long.parseLong(id), nome, cognome, nuovaMail, role, mail);		
		return "allUtenti";
	}
	
	@PostMapping("/deleteUtente")
	public @ResponseBody boolean deleteUtente(@RequestBody String id, Model model,
			HttpServletResponse response,
			@CookieValue("Mail") String mail,
			HttpServletRequest request) {
		
		Long idUtente = Long.parseLong(id.split("=")[1]);
		return utenteservice.deleteUtente(model, idUtente,  mail, response, request);
	}
	
	@PostMapping("/addFriend")
	public @ResponseBody boolean addFriend(@RequestBody String id,
			@CookieValue("Mail") String mail) {
			
		Long idUtente = Long.parseLong(id.split("=")[1]);
		return utenteservice.addFriend(idUtente, mail);
	}
	
	
	@RequestMapping(value="/rimuoviAmicizia", method=RequestMethod.POST)
	public String removeFriend(@CookieValue("Mail") String mail,
	@RequestParam(required = false) String id) {
		
		utenteservice.removeFriend(Long.parseLong(id), mail);
		return "redirect:/utente/cantina";
	}
	
	@RequestMapping(value="/cantina", method=RequestMethod.GET)
	public  String viewCantina(@CookieValue("Mail") String mail, Model model){
		
		utenteservice.viewCantina(mail, model);
		return "cantina";
	}
	
	@RequestMapping(value="/cantina", method=RequestMethod.POST)
	public  String viewCantinaPost(@CookieValue("Mail") String mail, Model model,
			@RequestParam(required = false) String id){

			if(id==null) {
				model.addAttribute("errore", "Errore nel caricamento della pagina");
			}else {
				Long idUtente = Long.parseLong(id);
				utenteservice.viewFriendCantina(model, mail, idUtente);
			}
			return "cantina";
	}
			
		
		
	
	
	@RequestMapping(value="/updateCantina", method=RequestMethod.POST)
	public @ResponseBody boolean updateBottiglie (@RequestBody String json,
			@CookieValue("Mail") String mail) throws InterruptedException, JsonMappingException, JsonProcessingException  {
		WrapperProdottoUtente tmp = null;
		List<WrapperProdottoUtente> prodotto = objectMapper.readValue(json, new TypeReference<List<WrapperProdottoUtente>>(){});

		return utenteservice.updateBottiglie( mail, prodotto);

	}
	
	
	@RequestMapping(value="/deleteProdotto", method=RequestMethod.POST)
	public @ResponseBody boolean deleteProdotto(@RequestBody String json, @CookieValue("Mail") String mail) 
			throws InterruptedException, JsonMappingException, JsonProcessingException  {

		WrapperProdottoUtente tmp = null;
		List<WrapperProdottoUtente> prodotto = objectMapper.readValue(json, new TypeReference<List<WrapperProdottoUtente>>(){});

		return utenteservice.deleteProdotto( mail, prodotto);
		
	}
	
	
	


}
