package com.lrf.controllers;


import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lrf.repository.CantinaProduttriceRepository;
import com.lrf.repository.UtenteVinoRepository;
import com.lrf.repository.RegioneRepository;
import com.lrf.repository.UtenteRepository;
import com.lrf.repository.VinoRepository;
import com.lrf.service.ProdottoService;
import com.lrf.service.UtenteService;

@Controller
@RequestMapping("/vino")
public class VinoController {
	
	@Autowired
	UtenteRepository utentiDB;
	
	@Autowired
	VinoRepository viniDB;
	
	@Autowired
	RegioneRepository regioniDB;
	
	@Autowired
	CantinaProduttriceRepository cantineProdDB;
	
	@Autowired
	UtenteVinoRepository utentiViniDB;
	
	@Autowired
    private ProdottoService service;
	
	@Autowired
	private UtenteService utenteService;
	
	@GetMapping("")
	public String listAll(Model model) {
		addAttributesToView(model);
		return "allVini";		
	}
	
    @PostMapping("/add")
    public String addVino(HttpServletResponse response, Model model,
			@RequestParam(name="nome", required=false) String nome, 
			@RequestParam(name="denominazione", required=false) String denominazione,
			@RequestParam(name="nazionalita", required=false) String nazionalita,
			@RequestParam(name="regione", required=false) String regione,
			@RequestParam(name="cantinaproduttrice", required=false) String cantinaProduttrice,
			@RequestParam(name="tipologia", required=false) String tipologia,
			@RequestParam(name="selectNazionalita") String select) {

		service.addVino(model, nome, denominazione, nazionalita, regione, cantinaProduttrice, tipologia, select);   	
    	addAttributesToView(model);
		return "allVini";
    }	
	

	@RequestMapping(value="/delete/{id}")
	public String delete(@CookieValue("Mail") String mail, @PathVariable("id") Long id, Model model) {
		
		if(utenteService.checkPermission(mail)) {
			service.deleteVino(id, model);
			addAttributesToView(model);
			return "allVini";
		}
		else {
			addAttributesToView(model);
			return "allVini";
		}
		
	}
	
	@PostMapping("addToWinecellar")
	public String addToWinecellar(@CookieValue("Mail") String mail, Model model, @RequestParam(name="nomevino") String nomevino, 
			@RequestParam(name="anno") String anno, @RequestParam(name="nbottiglie") String nbottiglie, @RequestParam(name="cantina") String cantina) {
		
		if(utenteService.checkPermission(mail)) {
			utenteService.addVinoToCantina(model, mail, nomevino, anno, nbottiglie, cantina);
			addAttributesToView(model);
			return "allVini";
		}else {
			setError(model, "Non sei loggato. Perfavore, vai alla pagina di login.");
			addAttributesToView(model);
			return "allVini";
		}		
		
	}
	

	@PostMapping("/edit")
	public String wineEdit(@CookieValue("Mail") String mail, Model model, @RequestParam(name="editNome") String nuovoNome, 
			@RequestParam(name="editDenominazione") String denominazione, @RequestParam(name="editNazionalita") String editNazionalita,
			@RequestParam(name="editRegione") String editRegione, @RequestParam(name="editCantinaProduttrice") String cantinaProduttrice,
			@RequestParam(name="editTipologia") String tipologia, @RequestParam(name="oldName") String oldNome,
			@RequestParam(name="oldCantina") String oldCantina,	@RequestParam(name="selectNazionalita") String select) {

		if(utenteService.checkPermission(mail)) {
			service.updateVino(model, nuovoNome, denominazione, editNazionalita, editRegione, cantinaProduttrice, tipologia, oldNome, oldCantina, select);
			addAttributesToView(model);
			return "allVini";
		}
		else {
			addAttributesToView(model);
			return "allVini";
		}
		
	}
	
    public final void addAttributesToView(Model model) {
    	model.addAttribute("vini", viniDB.findAll());
		model.addAttribute("regioni", regioniDB.findAll());
    }
    
    public final void setError(Model model, String message) {
		model.addAttribute("error", true);
		model.addAttribute("message", message);
	}
	
}