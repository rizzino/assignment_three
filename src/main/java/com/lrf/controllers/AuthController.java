package com.lrf.controllers;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lrf.domain.CantinaProduttrice;
import com.lrf.domain.Grappa;
import com.lrf.domain.Regione;
import com.lrf.domain.Utente;
import com.lrf.domain.UtenteGrappa;
import com.lrf.domain.UtenteVino;
import com.lrf.domain.Vino;
import com.lrf.repository.CantinaProduttriceRepository;
import com.lrf.repository.GrappaRepository;
import com.lrf.repository.RegioneRepository;
import com.lrf.repository.UtenteGrappaRepository;
import com.lrf.repository.UtenteRepository;
import com.lrf.repository.UtenteVinoRepository;
import com.lrf.repository.VinoRepository;
import com.lrf.service.UtenteService;

@Controller
public class AuthController {
	
	@Autowired
	private UtenteRepository utentiDB;
	
	@Autowired
	private RegioneRepository regioniDB;
	
	@Autowired
	private CantinaProduttriceRepository cantineProdDB;
	
	@Autowired
	private GrappaRepository grappeDB;
	
	@Autowired
	private UtenteGrappaRepository utentiGrappeDB;
	
	@Autowired
	private UtenteVinoRepository utentiViniDB;
	
	@Autowired
	private VinoRepository viniDB;
	
	@Autowired
	private UtenteService utenteservice;
	
	@GetMapping("/home")
	public String home(HttpServletRequest request, HttpServletResponse response,  Model model) {
		
		Cookie[] cookieArray = request.getCookies();

		if(cookieArray==null);
		else {
			for(int i=0; i<cookieArray.length; i++) {
				cookieArray[i].setMaxAge(0);
				response.addCookie(cookieArray[i]);
			}
		}
		Cookie cookie = new Cookie("Logged","false");
		response.addCookie(cookie);
		return "home";
	}
	
	@GetMapping("/login")
	public String getLogin() {		
		return "login";		
	}

    @PostMapping("/login")
    public String login(HttpServletResponse response, HttpServletRequest request, Model model,
    		@RequestParam(value = "Email", required = true) String mail) {    	
    	
    	Utente user = utenteservice.getUtente(model, mail);
    	if (user != null) {
    		Cookie cookieMail = new Cookie("Mail", mail);
    		cookieMail.setMaxAge(60*60*24); //Hours session
    		cookieMail.setPath("/");
    		response.addCookie(cookieMail);
    		Cookie cookieName = new Cookie("Name", user.getNome());
    		cookieName.setMaxAge(60*60*24); //Hours session
    		cookieName.setPath("/");
    		response.addCookie(cookieName);
    		Cookie cookieLastname= new Cookie("Lastname", user.getCognome());
    		cookieLastname.setMaxAge(60*60*24); //Hours session
    		cookieLastname.setPath("/");
    		response.addCookie(cookieLastname);
    		Cookie cookieLogged = new Cookie("Logged", "true");
    		cookieLogged.setMaxAge(60*60*24); //Hours session
    		cookieLogged.setPath("/");
            response.addCookie(cookieLogged);
            if(mail.equals("admin@gmail.com")) {
            	Cookie cookieRole = new Cookie("Role", "administrator");
        		cookieRole.setMaxAge(60*60*24); //Hours session
        		cookieRole.setPath("/");
                response.addCookie(cookieRole);
            }else {
            	Cookie cookieRole = new Cookie("Role", "user");
        		cookieRole.setMaxAge(60*60*24); //Hours session
        		cookieRole.setPath("/");
                response.addCookie(cookieRole);
            }
            return "redirect:/utente/cantina";
            
    	} else {
    		Cookie cookieLogged = new Cookie("Logged", "false");
    		cookieLogged.setMaxAge(60*60*24); //Hours session
    		cookieLogged.setPath("/");
            response.addCookie(cookieLogged);
            model.addAttribute("error", true);
            model.addAttribute("message", "Email di login non corretta!");
    	}
    	return "login";
    	
    }
    
    @PostMapping("/registration")
    public String Registration(
    		HttpServletResponse response,
    		Model model, 
    		@RequestParam(value = "name", required = true) String nome,
    		@RequestParam(value = "lastname", required = true) String cognome,
    		@RequestParam(value = "mail", required = true) String mail
    		){
    
    utenteservice.addUtente(model, nome, cognome, mail);
    
    return "home";    	
    }
    
	@Transactional
	@GetMapping("/popola")
	public void populateDB(HttpServletResponse response) throws IOException {
		
		/**
		 * Aggiunta regioni
		 */
		Regione regione1 = regioniDB.findByNome("Lombardia");
		if (regione1==null) {
			regione1 = new Regione("Lombardia");
			regioniDB.saveAndFlush(regione1);
		}
		Regione regione2 = regioniDB.findByNome("Trentino Alto Adige");
		if (regione2==null) {
			regione2 = new Regione("Trentino Alto Adige");
			regioniDB.saveAndFlush(regione2);
		}
		
		/**
		 * Aggiunta utenti
		 */
		Utente utente1 = utentiDB.findByMail("user@gmail.com");
		if(utente1==null) {
			utente1 = new Utente("Tizio","Caio","user@gmail.com");
			utentiDB.saveAndFlush(utente1);
		}
		Utente utente2 = utentiDB.findByMail("pippo.pluto@gmail.com");
		if(utente2==null) {
			utente2 = new Utente("Pippo","Pluto","pippo.pluto@gmail.com");
			utente2.getAmici().add(utente1);
			utentiDB.saveAndFlush(utente2);
		}		
		
		/**
		 * Aggiunta cantine produttrici
		 */
		CantinaProduttrice cantina1 = cantineProdDB.findByNome("MyCantinaProd");
		if (cantina1 == null) {
			cantina1 = new CantinaProduttrice("MyCantinaProd");
			cantineProdDB.saveAndFlush(cantina1);
		}
		CantinaProduttrice cantina2 = cantineProdDB.findByNome("Cantina Prod");
		if (cantina2 == null) {
			cantina2 = new CantinaProduttrice("Cantina Prod");
			cantineProdDB.saveAndFlush(cantina2);
		}
		
		/**
		 * Aggiunta vini
		 */
		Vino vino1 = viniDB.findByNome("Lambrusco");
		Vino vino2 = viniDB.findByNome("Barolo");				
	
		if (vino1 == null ) {
			vino1 = new Vino("Lambrusco", "", "Italiana", regione1, cantina1, "ROSSO");
			viniDB.saveAndFlush(vino1);
		}
		
		if (vino2 == null) {
			vino2 = new Vino("Barolo", "", "Italiana", regione2, cantina2, "ROSSO");
			viniDB.saveAndFlush(vino2);
		}
		
		/**
		 * Aggiunta grappe
		 */
		Grappa grappa1 = grappeDB.findByNome("Grappa1");
		Grappa grappa2 = grappeDB.findByNome("Grappa2");
		
		if(grappa1==null) {
			grappa1 = new Grappa("Grappa1","","Italiana", regione1, cantina1, "BARRICATA");
			grappeDB.saveAndFlush(grappa1);
		}
		if(grappa2==null) {
			grappa2 = new Grappa("Grappa2","","Italiana", regione2, cantina2, "BIANCA");
			grappeDB.saveAndFlush(grappa2);
		}
		
		/**
		 * Aggiunta vino alla cantina dell'utente
		 */
		UtenteVino myC1 = utentiViniDB.findByUtenteAndAnnoAndVino(utente1, 2002, vino1);
		if (myC1 == null) {
			myC1 = new UtenteVino(utente1, vino1, 2002, 3);
			utentiViniDB.saveAndFlush(myC1);
		}
		
		UtenteVino myC2 = utentiViniDB.findByUtenteAndAnnoAndVino(utente1, 2001, vino2);
		if (myC2 == null) {
			myC2 = new UtenteVino(utente1, vino2, 2001, 2);
			utentiViniDB.saveAndFlush(myC2);
		}
		
		UtenteVino myC3 = utentiViniDB.findByUtenteAndAnnoAndVino(utente2, 2002, vino1);
		if (myC3 == null) {
			myC3 = new UtenteVino(utente2, vino1, 2002, 3);
			utentiViniDB.saveAndFlush(myC3);
		}
		
		UtenteVino myC4 = utentiViniDB.findByUtenteAndAnnoAndVino(utente2, 2004, vino2);
		if (myC4 == null) {
			myC4 = new UtenteVino(utente2, vino2, 2004, 2);
			utentiViniDB.saveAndFlush(myC4);
		}
		
		/**
		 * Aggiunta grappe alla cantina dell'utente
		 */
		UtenteGrappa myC5 = utentiGrappeDB.findByUtenteAndAnnoAndGrappa(utente1, 2002, grappa1);
		if (myC5 == null) {
			myC5 = new UtenteGrappa(utente1, grappa1, 2002, 3);
			utentiGrappeDB.saveAndFlush(myC5);
		}
		
		UtenteGrappa myC6 = utentiGrappeDB.findByUtenteAndAnnoAndGrappa(utente1, 2001, grappa2);
		if (myC6 == null) {
			myC6 = new UtenteGrappa(utente1, grappa2, 2001, 2);
			utentiGrappeDB.saveAndFlush(myC6);
		}
		
		UtenteGrappa myC7 = utentiGrappeDB.findByUtenteAndAnnoAndGrappa(utente2, 2002, grappa1);
		if (myC7 == null) {
			myC7 = new UtenteGrappa(utente2, grappa1, 2002, 3);
			utentiGrappeDB.saveAndFlush(myC7);
		}
		
		UtenteGrappa myC8 = utentiGrappeDB.findByUtenteAndAnnoAndGrappa(utente2, 2004, grappa2);
		if (myC8 == null) {
			myC8 = new UtenteGrappa(utente2, grappa2, 2004, 2);
			utentiGrappeDB.saveAndFlush(myC8);
		}
	    
		response.sendRedirect("http://localhost:8080/home");	
	}
    
}