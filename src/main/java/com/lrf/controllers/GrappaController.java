package com.lrf.controllers;


import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lrf.repository.CantinaProduttriceRepository;
import com.lrf.repository.GrappaRepository;
import com.lrf.repository.RegioneRepository;
import com.lrf.repository.UtenteGrappaRepository;
import com.lrf.repository.UtenteRepository;
import com.lrf.service.ProdottoService;
import com.lrf.service.UtenteService;

@Controller
@RequestMapping("/grappa")
public class GrappaController {
	
	@Autowired
	UtenteRepository utentiDB;
	
	@Autowired
	GrappaRepository grappeDB;
	
	@Autowired
	RegioneRepository regioniDB;
	
	@Autowired
	CantinaProduttriceRepository cantineProdDB;
	
	@Autowired
	UtenteGrappaRepository utentiGrappeDB;
	
	@Autowired
    private ProdottoService service;
	
	@Autowired
	private UtenteService utenteService;
	
	@GetMapping("")
	public String listAll(Model model) {
		addAttributesToView(model);
		return "allGrappe";
	}
	
	@PostMapping("/add")
	public String addGrappa(HttpServletResponse response, Model model,
			@RequestParam(name="nome", required=false) String nome, 
			@RequestParam(name="denominazione", required=false) String denominazione,
			@RequestParam(name="nazionalita", required=false) String nazionalita,
			@RequestParam(name="regione", required=false) String regione,
			@RequestParam(name="cantinaproduttrice", required=false) String cantinaProduttrice,
			@RequestParam(name="tipologia", required=false) String tipologia,
			@RequestParam(name="selectNazionalita") String select) {
		
		service.addGrappa(model, nome, denominazione, nazionalita, regione, cantinaProduttrice, tipologia, select);
		addAttributesToView(model);
		return "allGrappe";
				
	}
	
	@RequestMapping(value="/delete/{id}")
	public String delete(@CookieValue("Mail") String mail, @PathVariable("id") Long id, Model model) {
		
		if(utenteService.checkPermission(mail)) {
			service.deleteGrappa(id, model);
			addAttributesToView(model);
			return "allGrappe";
		}
		else {
			addAttributesToView(model);
			return "allGrappe";
		}
			
	}
	
	@PostMapping("addToGrappacellar")
	public String addToGrappacellar(@CookieValue("Mail") String mail, Model model, @RequestParam(name="nomegrappa") String nomegrappa, 
			@RequestParam(name="anno") String anno, @RequestParam(name="nbottiglie") String nbottiglie, @RequestParam(name="cantina") String cantinaString) {
		
		if(utenteService.checkPermission(mail)) {
			utenteService.addToGrappacellar(model, mail, nomegrappa, anno, nbottiglie, cantinaString);
			addAttributesToView(model);
			return "allGrappe";
		}else {

			setError(model, "Non sei loggato. Perfavore, vai alla pagina di login.");
			addAttributesToView(model);
			return "allGrappe";
		}		
		
	}

	@PostMapping("/edit")
	public String grappaEdit(@CookieValue("Mail") String mail, Model model, @RequestParam(name="editNome") String nuovoNome, 
			@RequestParam(name="editDenominazione") String denominazione, @RequestParam(name="editNazionalita") String editNazionalita,
			@RequestParam(name="editRegione") String editRegione, @RequestParam(name="editCantinaProduttrice") String cantinaProduttrice,
			@RequestParam(name="editTipologia") String tipologia, @RequestParam(name="oldName") String oldNome,
			@RequestParam(name="oldCantina") String oldCantina, @RequestParam(name="selectNazionalita") String select) {
		
		if(utenteService.checkPermission(mail)) {
			service.updateGrappa(model, nuovoNome, denominazione, editNazionalita, editRegione, cantinaProduttrice, tipologia, oldNome, oldCantina, select);
			addAttributesToView(model);
			return "allGrappe";
		}
		else {
			addAttributesToView(model);
			return "allGrappe";
		}
		
}
	
    public final void addAttributesToView(Model model) {
    	model.addAttribute("grappe", grappeDB.findAll());
		model.addAttribute("regioni", regioniDB.findAll());
    }
    
    public final void setError(Model model, String message) {
    	model.addAttribute("error", true);
    	model.addAttribute("message", message);
    }
	
}