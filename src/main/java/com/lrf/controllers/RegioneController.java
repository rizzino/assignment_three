package com.lrf.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lrf.domain.Regione;
import com.lrf.repository.RegioneRepository;

@Controller
@RequestMapping("/regione")
public class RegioneController {
	
	@Autowired
	private RegioneRepository regioniDB;
	
	
	@Transactional
	@GetMapping("/popola")
	public String popola() {
		
		if(!regioniDB.existsByNome("Lombardia")) {
			Regione  reg = new Regione("Lombardia");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Valle d'Aosta")) {
			Regione  reg = new Regione("Valle d'Aosta");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Piemonte")) {
			Regione  reg = new Regione("Piemonte");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Liguria")) {
			Regione  reg = new Regione("Liguria");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Emilia Romagna")) {
			Regione  reg = new Regione("Emilia Romagna");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		
		if(!regioniDB.existsByNome("Veneto")) {
			Regione  reg = new Regione("Veneto");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Trentino Alto Adige")) {
			Regione  reg = new Regione("Trentino Alto Adige");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Friuli Venezia Giulia")) {
			Regione  reg = new Regione("Friuli Venezia Giulia");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Toscana")) {
			Regione  reg = new Regione("Toscana");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		
		if(!regioniDB.existsByNome("Marche")) {
			Regione  reg = new Regione("Marche");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Umbria")) {
			Regione  reg = new Regione("Umbria");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Lazio")) {
			Regione  reg = new Regione("Lazio");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Abruzzo")) {
			Regione  reg = new Regione("Abruzzo");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Molise")) {
			Regione  reg = new Regione("Molise");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Campania")) {
			Regione  reg = new Regione("Campania");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Basilicata")) {
			Regione  reg = new Regione("Basilicata");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Puglia")) {
			Regione  reg = new Regione("Puglia");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Calabria")) {
			Regione  reg = new Regione("Calabria");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Sicilia")) {
			Regione  reg = new Regione("Sicilia");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		if(!regioniDB.existsByNome("Sardegna")) {
			Regione  reg = new Regione("Sardegna");
			regioniDB.save(reg);
			regioniDB.flush();
		}
		
		
		return "redirect:/home";
		
		

	}

}
