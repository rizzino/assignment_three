package com.lrf.domain;

import java.util.EnumSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="vino")
public class Vino extends Prodotto {

	
	@Column(name="tipologiaVino")
	Enum<TipologiaVino> tipologiaVino;
	
	@JsonIgnore
	@OneToMany(mappedBy = "vino")
	private List<UtenteVino> vinoAssoc;
	
	public Vino() {}
	
	public Vino(String nome, String denominazione, String nazionalità, 
				Regione idReg, CantinaProduttrice idCant, String tipoVino) {
		
		super(nome, denominazione, nazionalità, idReg, idCant);	
		setTipologiaVino(tipoVino);
	}
	
	public List<UtenteVino> getVinoAssoc() {
		return vinoAssoc;
	}

	public void setVinoAssoc(List<UtenteVino> vinoAssoc) {
		this.vinoAssoc = vinoAssoc;
	}

	public void setTipologiaVino(Enum<TipologiaVino> tipologiaVino) {
		this.tipologiaVino = tipologiaVino;
	}

	public Enum<TipologiaVino> getTipologiaVino() {
		return tipologiaVino;
	}
	
	public boolean setTipologiaVino(String tipologiaVino) {
		boolean flag = false;
		for(TipologiaVino tipo: TipologiaVino.values()) {
			if(tipo.toString().equals(tipologiaVino)) {
				this.tipologiaVino = tipo;
				flag = true;
			}
		}
		return flag;
	}

	
    public boolean isPresentTipologia (String name) {
		for (TipologiaVino value : EnumSet.allOf(TipologiaVino.class)) {
			 if (value.name().equalsIgnoreCase(name)) {
				 return true;
			 }
		}
    	 return false;
     }

}