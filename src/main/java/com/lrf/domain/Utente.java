package com.lrf.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="Utente")
public class Utente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@NotBlank
	@Column(name="Nome")
	private String nome;
	
	@NotBlank
	@Column(name="Cognome")
	private String cognome;
	
	@Email
	@NotBlank
	@Column(unique = true, name="Mail")
	private String mail;
	
	public Long getId() {
		return id;
	}

	@ManyToMany()
	@JoinTable(name="utente_amico",
		joinColumns={@JoinColumn(name="id_utente")},
		inverseJoinColumns={@JoinColumn(name="id_amico")})
	@JsonIgnore	
	private List<Utente> amici = new ArrayList<Utente>();

	@ManyToMany(mappedBy="amici")
	@JsonIgnore
	private List<Utente> allUtenti = new ArrayList<Utente>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "utente", cascade = CascadeType.REMOVE)
	private List<UtenteVino> utenteVinoAssoc;
	
	@JsonIgnore
	@OneToMany(mappedBy = "utente", cascade = CascadeType.REMOVE)
	private List<UtenteGrappa> utenteGrappaAssoc;
	
	public Utente() {}
	
	public Utente(String nome, String cognome, String mail) {
		setNome(nome);
		setCognome(cognome);
		setMail(mail);
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public List<Utente> getAmici() {
		return amici;
	}

	public void setAmici(List<Utente> amici) {
		this.amici = amici;
	}

	public List<Utente> getAllUtenti() {
		return allUtenti;
	}

	public void setAllUtenti(List<Utente> allUtenti) {
		this.allUtenti = allUtenti;
	}

	public List<UtenteVino> getUtenteVinoAssoc() {
		return utenteVinoAssoc;
	}

	public void setUtenteVinoAssoc(List<UtenteVino> utenteVinoAssoc) {
		this.utenteVinoAssoc = utenteVinoAssoc;
	}

	public List<UtenteGrappa> getUtenteGrappaAssoc() {
		return utenteGrappaAssoc;
	}

	public void setUtenteGrappaAssoc(List<UtenteGrappa> utenteGrappaAssoc) {
		this.utenteGrappaAssoc = utenteGrappaAssoc;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}


}
