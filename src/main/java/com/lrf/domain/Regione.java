package com.lrf.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;


@Entity
@Table(name="regione")
public class Regione {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idr")
	private Long idr;
	
	@NotBlank
	@Column(unique =true, name="NomeRegione")
	private String nome;
	
	@OneToMany(mappedBy = "idRegione")
	private List<Prodotto> prodotti;
	
	public Regione() {}
	
	public Regione(String nome) {
		setNome(nome);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Long getIdr() {
		return idr;
	}
	
	public void setIdr(Long idr) {
		this.idr = idr;
	}
	
}