package com.lrf.domain;

import java.util.EnumSet;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="grappa")
public class Grappa extends Prodotto {
 
	@Column(name="tipologiaGrappa")
	Enum<TipologiaGrappa> tipologiaGrappa;
	
	@JsonIgnore
	@OneToMany(mappedBy = "grappa")
	private List<UtenteGrappa> grappaAssoc;
	
	public Grappa() {};
	 
	public Grappa(String nome, String denominazione, String nazionalità, 
				Regione idReg, CantinaProduttrice idCant, String tipoGrappa) {
		
		super(nome, denominazione, nazionalità, idReg, idCant);
		setTipologiaGrappa(tipoGrappa);
	}
	
	public List<UtenteGrappa> getGrappaAssoc() {
		return grappaAssoc;
	}
	
	public void setGrappaAssoc(List<UtenteGrappa> grappaAssoc) {
		this.grappaAssoc = grappaAssoc;
	}
	
	public void setTipologiaGrappa(Enum<TipologiaGrappa> tipologiaGrappa) {
		this.tipologiaGrappa = tipologiaGrappa;
	}
	
	public Enum<TipologiaGrappa> getTipologiaGrappa() {
		return tipologiaGrappa;
	}
	
	public boolean setTipologiaGrappa(String tipologiaGrappa) {
		boolean flag = false;
		for(TipologiaGrappa tipo: TipologiaGrappa.values()) {
			 if(tipo.toString().equals(tipologiaGrappa)){
				 this.tipologiaGrappa = tipo;
				 flag = true;
			 }
		}
		return flag;
	}
	
	public boolean isPresentTipologia (String name) {
		for (TipologiaGrappa value : EnumSet.allOf(TipologiaGrappa.class)) {
			 if (value.name().equalsIgnoreCase(name)) {
				 return true;
			 }
		}
		return false;
	 }
	
	 
}
