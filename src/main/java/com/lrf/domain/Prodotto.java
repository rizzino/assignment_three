package com.lrf.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;

import javax.validation.constraints.NotBlank;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;


@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Prodotto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idprodotto")
	private Long id;
	
	@NotBlank
	@Column(name="Nome")
	private String nome;
	

	@Column(name="Denominazione")
	private String denominazione;	
	
	@NotBlank
	@Column(name="Nazionalità")
	private String nazionalita;	
	

	//@JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="regione_id", referencedColumnName = "idr", nullable=true)
	private Regione idRegione;
	
	//@NotBlank
	//@JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="cantina_id", referencedColumnName = "idc", nullable=false)
	private CantinaProduttrice idCantina;
	
	public Prodotto(String nome, String denominazione, String nazionalità, 
			Regione idReg, CantinaProduttrice idCant) {
		setNome(nome);
		setDenominazione(denominazione);
		setNazionalita(nazionalità);
		setIdRegione(idReg);
		setIdCantina(idCant);
	}

	public Prodotto() {}
	
	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDenominazione() {
		return denominazione;
	}

	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}

	public String getNazionalita() {
		return nazionalita;
	}

	public void setNazionalita(String nazionalita) {
		this.nazionalita = nazionalita;
	}

	public Regione getIdRegione() {
		return idRegione;
	}

	public void setIdRegione(Regione idRegione) {
		this.idRegione = idRegione;
	}

	public CantinaProduttrice getIdCantina() {
		return idCantina;
	}

	public void setIdCantina(CantinaProduttrice idCantina) {
		this.idCantina = idCantina;
	}
	
}
