package com.lrf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lrf.domain.Regione;

public interface RegioneRepository extends JpaRepository<Regione, Long> {
	
	boolean existsByNome(String nome);
	Regione findByNome(String nome);
	
}
