package com.lrf.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lrf.domain.Utente;
import com.lrf.domain.UtenteVino;
import com.lrf.domain.Vino;

public interface UtenteVinoRepository extends JpaRepository<UtenteVino, Long> {
	
	boolean existsById(Long id);
	Optional<UtenteVino> findById(Long id);
	List<UtenteVino> findByAnno(int anno);
	List<UtenteVino> findByVino(Vino vino);
	List<UtenteVino> findByUtente(Utente utente);
	UtenteVino findByUtenteAndAnnoAndVino(Utente user, int Anno, Vino vino);
	
}
