package com.lrf.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lrf.domain.Vino;
import com.lrf.domain.CantinaProduttrice;
import com.lrf.domain.TipologiaVino;

@Repository
public interface VinoRepository extends JpaRepository<Vino, Long> {
	
	boolean existsById(Long id);
	boolean existsByTipologiaVino(Enum<TipologiaVino> tipologia);
	List<Vino> findByTipologiaVino(Enum<TipologiaVino> tipologia);
	Vino findByNome(String nome);
	Optional<Vino> findById(Long Id);
	Vino findByNomeAndIdCantina(String nome, CantinaProduttrice cantina);
	
}